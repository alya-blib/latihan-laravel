<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
      <label>First name:</label> <br />
      <br />
      @csrf
      <input type="text" name="nama_depan" /> <br />
      <br />
      <label>Last name:</label> <br />
      <br />
      <input type="text" name="nama_belakang" /> <br />
      <br />
      <label>Gender:</label> <br />
      <br />
      <input type="radio" name="Male" /> Male <br />
      <input type="radio" name="Female" /> Female <br />
      <input type="radio" name="Other" /> Other <br />
      <br />
      <label>Nationality:</label> <br />
      <br />
      <select name="N">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Korea Selatan">Korea Selatan</option>
      </select>
      <br />
      <br />
      <label>Language Spoken:</label> <br />
      <br />
      <input type="checkbox" /> Bahasa Indonesia <br />
      <input type="checkbox" /> English <br />
      <input type="checkbox" /> Other <br />
      <br />
      <label>Bio:</label> <br />
      <br />
      <textarea name="Bio" cols="30" rows="10"></textarea> <br />
      <input type="submit" />
    </form>
  </body>
</html>
