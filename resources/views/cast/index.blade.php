@extends ('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif
                  <a class="btn btn-primary mb-2" href="{{ route('cast.create') }}">Create New Post</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>umur</th>
                      <th>bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   @forelse($cast as $key => $cast)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $cast->nama }} </td>
                        <td> {{ $cast->umur }} </td>
                        <td> {{ $cast->bio }} </td>
                        <td style="display: flex;">
                            <a href=" {{ route('cast.show', ['cast' => $cast->id])}} " class="btn btn-info btn-sm">show</a>
                            <a href="/cast/{{ $cast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                           <form action="/cast/{{ $cast->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                           </form>
                        
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Cast</td>
                    </tr>
                    
                   @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
          
            </div>
    </div>
@endsection