<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmm', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->longText('ringkasan');
            $table->integer('tahun');
            $table->string('poster');
            $table->unsignedBigInteger('genree_id');
            $table->foreign('genree_id')->references('id')->on('genree');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmm');
        $table->dropForeign(['genree_id']);
        $table->dropColumn(['genree_id']);
    }
}
